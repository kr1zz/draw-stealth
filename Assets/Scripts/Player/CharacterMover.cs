using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
/// <summary>
/// Обеспечивает движение игрока по заданным точкам
/// </summary>
public class CharacterMover : MonoBehaviour
{
    [SerializeField] private float speedCharacter = 0.5f;
    [SerializeField] private Path path;
    [SerializeField] private LevelProgressTracker levelProgressTracker;
    [SerializeField]
    private Animator playerAnimator;

    private Vector3 startPosition;
    private int lastPoint;
    private Vector3 currentPossition;
    private Vector3 direction;
    private bool isMoving = false;


    private void Awake()
    {
        
    }

    private void Start()
    {
        startPosition = path.StartBush.position;
        transform.position = startPosition;
    }

    private void FixedUpdate()
    {

        if (isMoving == true)
        {
            Movement();
        }
        
        
    }   

    public void Movement()
    {
        float distance = speedCharacter * Time.fixedDeltaTime;
        while (distance >= (transform.position - path.PathPoints[lastPoint + 1]).magnitude && lastPoint < path.PathPoints.Count -2)
        {
            lastPoint++;
            
        }
        
        if (Vector3.Distance(transform.position, path.PathPoints.Last()) <= distance)
        {
            transform.position = path.PathPoints.Last();
            BushReached();
            
            return;
        }
        
        direction = path.PathPoints[lastPoint + 1] - transform.position;
        direction = direction.normalized * distance;
        Rotate();
        transform.Translate(direction, Space.World);
        

    }

    public void StartMoving()
    {
        lastPoint = 0;
        isMoving = true;
        playerAnimator.SetBool("isMoving", true);

    }

    public void EndMoving()
    {
        playerAnimator.SetBool("isMoving", false);
        isMoving = false;
    }

    public void BushReached()
    {
        levelProgressTracker.CompleteStage();
        EndMoving();
    }
    public void Rotate()
    {
        transform.LookAt(path.PathPoints[lastPoint + 1]);
    }

    public void ResetPosition()
    {
        transform.position = path.Bushes[0].transform.position;
    }
}
