using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MetallDetector : Enemy
{
    [SerializeField] private Collider detectorCollider;
    [SerializeField] private Color onColor;
    [SerializeField] private Color offColor;
    [SerializeField] private MeshRenderer firstPillarRenderer;
    [SerializeField] private MeshRenderer secondPillarRenderer;
    [SerializeField] private float onStateDuration = 2f;
    [SerializeField] private float offStateDuration = 1f;
    
    private Material pillarMaterial;
    private bool isDetectorActive = true;

    private IEnumerator DetectorPattern()
    {
        if (detectorCollider.enabled) Toggle();
        while (isDetectorActive)
        {
            Toggle();
            yield return new WaitForSeconds(onStateDuration);
            Toggle();
            yield return new WaitForSeconds(offStateDuration);
        }
    }
    
    private void Awake()
    {
        pillarMaterial = firstPillarRenderer.material;
        secondPillarRenderer.material = pillarMaterial;
        pillarMaterial.color = onColor;
        StartCoroutine(DetectorPattern());
    }

    public void Toggle()
    {
        detectorCollider.enabled = !detectorCollider.enabled;
        if (detectorCollider.enabled)
        {
            pillarMaterial.color = onColor;
        }
        else
        {
            pillarMaterial.color = offColor;
        }
    }
    
}
