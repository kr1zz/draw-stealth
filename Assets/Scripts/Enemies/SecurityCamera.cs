using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SecurityCamera : MonoBehaviour
{
    [SerializeField] private GameObject fovDetector;
    [SerializeField] private Transform securityCameraTransform;
    [Range(0,360)]
    [SerializeField] private float leftAngle = 15f;
    [Range(0,360)]
    [SerializeField] private float rightAngle = 15f;
    [SerializeField] private bool isEnabled = true;
    [SerializeField] private bool isRightTurn = true;
    [SerializeField] private float rotationSpeed = 1f;
    
    private float currentAngle = 0f;

    private void Start()
    {
        leftAngle = -leftAngle;
    }

    private void FixedUpdate()
    {
        if (!isEnabled)
        {
            return;
        }

        float rotationAngle = Time.fixedDeltaTime * rotationSpeed * (isRightTurn ? 1 : -1);
        currentAngle += rotationAngle;
        if (isRightTurn && currentAngle >= rightAngle)
        {
            isRightTurn = false;
        }

        if (!isRightTurn && currentAngle <= leftAngle)
        {
            isRightTurn = true;
        }
        securityCameraTransform.Rotate(0, rotationAngle, 0);
    }

    public void Toggle()
    {
        isEnabled = !isEnabled;
        fovDetector.SetActive(isEnabled);
    }
}
