using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : InitializableWithCollider
{
    [SerializeField] private List<Detector> detectors;

    public Action OnPlayerDetected;
    

    public override void Initialize(Collider targetCollider)
    {
        foreach (Detector detector in detectors)
        {
            detector.TargetCollider = targetCollider;
            detector.OnDetected += DetectPlayer;
        }
    }

    private void OnDestroy()
    {
        foreach (Detector detector in detectors)
        {
            detector.OnDetected -= OnPlayerDetected;
        }
    }

    private void DetectPlayer()
    {
        OnPlayerDetected?.Invoke();
    }
}
