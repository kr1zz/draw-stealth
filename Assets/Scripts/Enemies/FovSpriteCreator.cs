using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.U2D;
/// <summary>
/// Отрисовывает поле зрения
/// </summary>
public class FovSpriteCreator : MonoBehaviour
{
    [SerializeField] private SpriteShapeController spriteShapeController;
    
    [SerializeField] private Transform mask;

    
    private Vector3 direction;
    private Vector3 position;

    


    public void DrawShape(float distance, float angle)
    {
        int vertexIndex = 0;
        direction = Vector3.up * distance * 2;
        Vector3 point = Quaternion.Euler(0, 0, 360 - angle/2) * direction;
        mask.localScale = Vector3.one * distance;
        spriteShapeController.spline.Clear();
        spriteShapeController.spline.InsertPointAt(vertexIndex++, Vector3.zero);
        spriteShapeController.spline.InsertPointAt(vertexIndex++, point);
        if (angle > 90)
        {
            print(point);

            point = Quaternion.Euler(0, 0, 90) * point;
            spriteShapeController.spline.InsertPointAt(vertexIndex++, point);
        }

        if (angle > 180)
        {
            print(point);

            point = Quaternion.Euler(0, 0, 90) * point;
            spriteShapeController.spline.InsertPointAt(vertexIndex++, point);
        }
        if (angle > 270)
        {
            print(point);

            point = Quaternion.Euler(0, 0, 90) * point;
            spriteShapeController.spline.InsertPointAt(vertexIndex++, point);
        }
        
        point = Quaternion.Euler(0, 0, angle/2) * direction;
        spriteShapeController.spline.InsertPointAt(vertexIndex++, point);
        //_spriteShapeController.spline.SetRightTangent(1, Vector3.right+Vector3.up);
        
    }
}
