using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    /// <summary>
    /// Перемещает врагов по заданным точкам
    /// </summary>
public class EnemyMover : MonoBehaviour
{
    [SerializeField] private List<Vector3> points;

    [SerializeField] private float enemySpeed = 1f;
    [SerializeField] private Animator enemyAnimator;

    private int lastPoint;

    private Transform enemyTransform;

    private void Awake()
    {
        enemyTransform = transform;
        lastPoint = 0;
        enemyTransform.position = points[lastPoint];
        for (int i = 0; i < points.Count - 1; i++)
        {
            Debug.DrawLine(points[i], points[i+1], Color.red, float.MaxValue);
        }


    }

    private void FixedUpdate()
    {
        Movement();
    }

    private void Movement()
    {
        if (lastPoint <= points.Count - 1)
        {
            enemyAnimator.SetBool("isMoving", true);
            float distance = enemySpeed * Time.fixedDeltaTime;
            while (distance >= (transform.position - points[lastPoint + 1]).magnitude && lastPoint < points.Count - 2)
            {
                lastPoint++;

            }

            if (Vector3.Distance(transform.position, points[points.Count - 1]) <= distance)
            {
                transform.position = points[points.Count - 1];
                points.Reverse();
                lastPoint = 0;


                return;
            }

            Vector3 positionToGo = points[lastPoint + 1];
            Vector3 direction = (positionToGo - enemyTransform.position).normalized;
            direction *= distance;
            Rotate();
            transform.Translate(direction, Space.World);

        }

    }

    private void Rotate()
    {
        transform.LookAt(points[lastPoint + 1]);
    }

    private void ReverseList()
    {
    }

}
