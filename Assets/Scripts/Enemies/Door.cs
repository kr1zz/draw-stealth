using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Enemy
{
    [SerializeField] private GameObject door;

    public void Toggle()
    {
            door.SetActive(!door.activeSelf);
    }
}

