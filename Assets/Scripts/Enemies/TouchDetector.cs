using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchDetector : Detector
{
    
    
    private void OnTriggerEnter(Collider other)
    {
        if (other == TargetCollider)
        {
            OnDetected?.Invoke();
        }
    }
}
