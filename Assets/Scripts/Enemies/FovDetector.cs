using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Позволяет обнаружить игрока в заданном поле зрения
/// </summary>
public class FovDetector : Detector
{
    [SerializeField] private float distance = 15f;
    [SerializeField] private float angle = 20;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private FovSpriteCreator fovSpriteCreator;
    private Transform sourceTransform;
    private Transform targetTransform;

    private Collider targetCollider;

    public override Collider TargetCollider
    {
        protected get=> targetCollider;
        set
        {
            
            targetCollider = value;
            targetTransform = TargetCollider.transform;

        }
    }


    private void Start()
    {
        fovSpriteCreator.DrawShape(distance, angle);
        sourceTransform = transform;
    }


    private void FixedUpdate ()
    {
         DetectTarget();
    }

    

    public void DetectTarget()
    {
        if (TargetCollider == null) return;
        Vector3 vectorToPlayer = targetTransform.position - sourceTransform.position;
        
        if(Vector3.Distance(sourceTransform.position, targetTransform.position) < distance)
        {
            if (Vector3.Angle((vectorToPlayer), sourceTransform.forward) < angle/2)
            {
                var ray = new Ray(sourceTransform.position, vectorToPlayer);
                if (Physics.Raycast(ray, out var hit, distance, layerMask))
                {
                    if (hit.transform == targetTransform)
                    {
                        OnDetected?.Invoke();
                    }
                }
            }
        }
    }
}
