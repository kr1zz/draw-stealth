using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Вызывает события при контакте с игроком
/// </summary>
public class FloorButton : InitializableWithCollider
{
    [SerializeField] private Collider playerCollider;
    [SerializeField] private Color onColor;
    [SerializeField] private Color offColor;
    [SerializeField] private UnityEvent OnToggle;

    private Material buttonMaterial;
    private bool isToggle;
    private void Start()
    {
        buttonMaterial = GetComponent<MeshRenderer>().material;
        buttonMaterial.color = onColor;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == playerCollider)
        {
            OnToggle?.Invoke();
            isToggle = !isToggle;
            if (isToggle) buttonMaterial.color = offColor;
            else buttonMaterial.color = onColor;
        }
    }

    public override void Initialize(Collider targetCollider)
    {
        playerCollider = targetCollider;
    }
}
