using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Вращает объект
/// </summary>
public class Rotator : MonoBehaviour
{
    [Range(0, 10)] [SerializeField] private float rotateSpeed = 1f;
    private float RotateSpeed => rotateSpeed * (isRightTurn ? 1 : -1);

    [SerializeField] private bool isRightTurn = true;
    
    private Vector3 turnDirection = new Vector3(0, 1,0 );

    private void FixedUpdate()
    {
        transform.Rotate(turnDirection * RotateSpeed);
    }

    
}