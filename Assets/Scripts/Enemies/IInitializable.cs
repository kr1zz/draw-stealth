using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInitializable<T>
{
    public void Initialize(T arg);
}
