using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private LevelsProgression levelsProgression;
    [SerializeField] private UIManager uiManager;
    [SerializeField] private LevelProgressTracker levelProgressTracker;
    [SerializeField] private CharacterMover characterMover;
    [SerializeField] private Path path;
    [SerializeField] private CameraMover cameraMover;
    [SerializeField] private GameObject currentLevel;
    [SerializeField] private Collider playerCollider;

    private void Awake()
    {
        LoadLevel();
    }

    private void OnEnable()
    {
        levelProgressTracker.OnLevelCompleted += WinGame;
    }

    public void WinGame()
    {
        levelsProgression.LastLevel++;
        
        uiManager.ShowWinPanel();
        
    }
    
    public void LoseGame()
    {
        characterMover.EndMoving();
        uiManager.ShowLosePanel();
        
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1;
    }

    public void LoadLevel()
    {
        int levelIndex = levelsProgression.LastLevel;
        if (currentLevel != null)
        {
            currentLevel.GetComponent<GameLevel>().OnLoseGame -= LoseGame;
            Destroy(currentLevel);
                        
        }
        currentLevel = Instantiate(levelsProgression.GetLevel(levelIndex));
        var gameLevel = currentLevel.GetComponent<GameLevel>();
        gameLevel.OnLoseGame += LoseGame;
        gameLevel.Initialize(playerCollider, path);
        var levelContentProvider = currentLevel.GetComponent<LevelContentProvider>();
        path.Bushes = levelContentProvider.Bushes;
        path.ResetPath();
        cameraMover.ResetCameraPosition();
        characterMover.ResetPosition();
    }
    public void RestartLevel()
    {
        LoadLevel();
    }
    
}
