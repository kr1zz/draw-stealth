using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
/// <summary>
/// Управляет Ui элементами
/// </summary>
public class UIManager : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [Header("Gameplay Components")]
    [SerializeField] private LevelProgressTracker levelProgressTracker;

    [SerializeField] private Path path;
    
    
    [Header("UI Components")]
    [SerializeField] private GameObject winPanel;

    [SerializeField] private GameObject startMovingButton;

    [SerializeField] private GameObject losePanel;
    [SerializeField] private Slider progressBar;

    private void OnEnable()
    {
        path.OnStartDrawing += HideStartMovingButton;
        path.OnDrawingCompleted += ShowStartMovingButton;
    }

    private void OnDisable()
    {   
        path.OnStartDrawing -= HideStartMovingButton;
        path.OnDrawingCompleted -= ShowStartMovingButton;
    }

    public void ShowWinPanel()
    {
        winPanel.SetActive(true);
    }

    private void ShowStartMovingButton()
    {
        startMovingButton.SetActive(true);
    }

    private void HideStartMovingButton()
    {
        startMovingButton.SetActive(false);
    }

    public void ShowLosePanel()
    {
        losePanel.SetActive(true);
    }

    private void ChangeProgressBarValue()
    {
        progressBar.value = levelProgressTracker.TrackProgress();
    }

    private void FixedUpdate()
    {
        ChangeProgressBarValue();
    }
    
    public void PauseGame()
    {
        gameManager.PauseGame();
    }

    public void UnpauseGame()
    {
        gameManager.UnpauseGame();
    }
}
