using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
///  Инициализирует объекты на уровне
/// </summary>
public class GameLevel : MonoBehaviour
{
    [SerializeField] private List<InitializableWithCollider> initalizableObjects;
    [SerializeField] private TouchHandler touchHandler;
    public Action OnLoseGame;
    

    public void Initialize(Collider playerCollider, Path path)
    {
        foreach (var initializableWithCollider in initalizableObjects)
        {
            initializableWithCollider.Initialize(playerCollider);
            if (initializableWithCollider is Enemy enemy)
            {
                enemy.OnPlayerDetected += OnLoseGame;

            }
        }

        touchHandler.Path = path;

    }


    private void OnDestroy()
    {
        foreach (var initializableWithCollider in initalizableObjects)
        {
            if (initializableWithCollider is Enemy enemy)
            {
                enemy.OnPlayerDetected -= OnLoseGame;

            }
        }
    }
    
    
}
