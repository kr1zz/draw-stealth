using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
/// <summary>
/// Построение пути по точкам
/// </summary>
public class Path : MonoBehaviour
{
    
    [SerializeField] private LineParticle lineParticle;
    [SerializeField] private float maxDistanceBetweenPoint = 0.2f;
    [SerializeField] private List<Transform> bushes;
    
    
    private float bushArea = 1f;
    private Transform startBush;
    private Transform finishBush;
    private int nextBushIndex;
    public Transform StartBush => startBush;
    
    private List<Vector3> pathPoints = new List<Vector3>();
    private bool isDrawing = false;
    private bool isLineConfirmed = false;

    public Action OnDrawingCompleted;
    public Action OnStartDrawing;
    private Vector3 LastPathPoint => pathPoints[pathPoints.Count - 1];

    public List<Transform> Bushes
    {
        get => bushes;
        set
        {
            bushes = value;
            if (bushes != null)
            {
                bushArea = bushes[0].lossyScale.x / 2;
            }
        }
        
    }

    public List<Vector3> PathPoints
    {
        get => pathPoints;
    }

    private void Awake()
    {
        
        ResetBushes();
    }

    private void Start()
    {
        lineParticle.SetStartPosition(bushes[0].transform.position); 
    }

    /// <summary>
    /// Добавляет новую точку в путь
    /// </summary>
    /// <param name="point"></param>
    public void AddPoint(Vector3 point)
    {
        
        if (isDrawing)
        {
            if (pathPoints.Count > 0 && Vector3.Distance(point, LastPathPoint) > maxDistanceBetweenPoint)
            {
                AddIntermediatePoints(point); 
            }
            else
            {
                pathPoints.Add(point);
                lineParticle.AddPoint(point);
                Vector3 bushPosition = finishBush.position + Vector3.up * 0.1f;
                if (IsPointInBushArea(point, bushPosition))
                {
                    StopDrawing();
                    pathPoints.Add(bushPosition);
                    lineParticle.AddPoint(bushPosition);
                    OnDrawingCompleted?.Invoke();

                }
            }
           
            
        }
       
    }
    /// <summary>
    /// Добавление нескольких точек в путь
    /// </summary>
    /// <param name="points"></param>
    public void AddPoints(List<Vector3> points)
    {
        if (isDrawing)
        {
            pathPoints.AddRange(points);
            lineParticle.AddPoints(points);
            if (IsPointInBushArea(points.Last(), finishBush.position))
            {
                StopDrawing();
                OnDrawingCompleted?.Invoke();
            }
            
            
        }
        

    }
    /// <summary>
    /// Добавление промежуточных точек, при большом расстоянии между точками
    /// </summary>
    /// <param name="point"></param>
    public void AddIntermediatePoints(Vector3 point)
    {
        
        List<Vector3> newPoints = new List<Vector3>();
        Vector3 direction = point - LastPathPoint;
        float distance = direction.magnitude;
        if (distance <= maxDistanceBetweenPoint) return;
        newPoints.Add(LastPathPoint + direction.normalized * (maxDistanceBetweenPoint));
        distance -= maxDistanceBetweenPoint;
        while (distance > maxDistanceBetweenPoint)
        {   
            
            newPoints.Add(newPoints.Last() + direction.normalized * (maxDistanceBetweenPoint));
            distance -= maxDistanceBetweenPoint;
            if (IsPointInBushArea(newPoints.Last(), finishBush.position))
            {
                newPoints.Add(finishBush.position + Vector3.up * 0.1f);
                AddPoints(newPoints);
                return;
            }

        }

       
        
        if (IsPointInBushArea(point, finishBush.position))
        {
            newPoints.Add(finishBush.position + Vector3.up * 0.1f);
        }
        else
        {
            newPoints.Add(point);
        }

        AddPoints(newPoints);
    }
    
    
    /// <summary>
    /// Очищает путь
    /// </summary>
    public void ClearPath() //
    {
        pathPoints.Clear();
        lineParticle.DeletePoints(bushes[nextBushIndex-1].position);
        StopDrawing();
    }
    
    
    
    /// <summary>
    /// Выполняет действия перед рисованием пути
    /// </summary>
    /// <param name="point"></param>
    public void OnStartPathDrawing(Vector3 point)
    {
        if (isLineConfirmed){return;}
        if (IsPointInBushArea(point, startBush.position))
        {
            ClearPath();
            OnStartDrawing?.Invoke();
            isDrawing = true;
        }
    }

    public bool IsPointInBushArea(Vector3 point, Vector3 bushPosition)
    {
        return Vector3.Distance(point, bushPosition) < bushArea;
        
    }

    public void StopDrawing()
    {
        isDrawing = false;
    }

    public void ConfirmLine()
    {
        isLineConfirmed = true;
    }

    public void ResetLine()
    {
        ClearPath();
        isLineConfirmed = false;
        StopDrawing();
    }

    public bool IsExistNextBush()
    {
        return (bushes.Count > nextBushIndex + 1);
    }

    public bool IsNextBushLast()
    {
        return (nextBushIndex == bushes.Count - 1);
    }
    
    public void SetNextBush()
    {
        startBush = finishBush;
        finishBush = bushes[++nextBushIndex];
    }

    public void ResetBushes()
    {
        startBush = bushes[0].transform;
        finishBush = bushes[1].transform;
        nextBushIndex = 1;
    }

    public void ResetPath()
    {
        ResetBushes();
        ResetLine();
        
    }
}
