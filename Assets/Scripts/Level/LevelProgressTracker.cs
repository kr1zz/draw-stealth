using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
/// <summary>
/// Отслеживает прогресс прохождения уровня 
/// </summary>
public class LevelProgressTracker : MonoBehaviour
{
    [SerializeField] private CameraMover cameraMover;
    [SerializeField] private Path path;
    [SerializeField] private Transform characterTransform;
    private float startPointZ;
    private float finishPointZ;
    private float levelLength;
    public Action OnLevelCompleted;

    

    private void Start()
    {
        startPointZ = path.StartBush.position.z;
        finishPointZ = path.Bushes[path.Bushes.Count - 1].position.z;
        levelLength = finishPointZ - startPointZ;

    }

    public void CompleteStage()
    {
        if (path.IsExistNextBush())
        {
            path.ResetLine();
            cameraMover.MoveToNextStage();
            path.SetNextBush();
            if (path.IsNextBushLast()) cameraMover.LockCamera();
        }
        else CompleteLevel();

        
        
    }

    private void CompleteLevel()
    {
        
        OnLevelCompleted?.Invoke();
    }

    public float TrackProgress()
    {
        
        var progress = (float)Math.Round(((characterTransform.position.z - startPointZ) /
                                 (levelLength)) * 100);
        
        return (progress);
    }
    
    


}
