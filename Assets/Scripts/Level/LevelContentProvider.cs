using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Предоставляет доступ к компонентам на уровне
/// </summary>
public class LevelContentProvider : MonoBehaviour
{
    [SerializeField] private List<Transform> bushes;

    public List<Transform> Bushes => bushes;
}
