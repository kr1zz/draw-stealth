using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
//Двигает камеру за персонажем
public class CameraMover : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private float stageDistance;
    [SerializeField] private float cameraSpeed = 2f;
    private Camera camera;
    private float startZPosition;
    private float currentStartZPosition;
    private float newZPosition;
    private Transform cameraTransform;
    private Vector3 currentCameraPosition;
    private bool isLocked = false;

    private void Awake()
    {
        camera = Camera.main;
        cameraTransform = camera.transform;
        startZPosition = cameraTransform.position.z;
        currentStartZPosition = startZPosition;
        currentCameraPosition = cameraTransform.position;
    }
    

    private void LateUpdate()
    {
        if ( !isLocked && camera.WorldToScreenPoint(playerTransform.position).y >= Screen.height / 2f)
        {
            currentCameraPosition.z = Mathf.Lerp(transform.position.z, playerTransform.position.z, 0.01f);
            transform.position = currentCameraPosition;
        }
        
        
    }

    private IEnumerator MoveCamera(float zPosition, float speed)
    {
        float distance = speed * Time.deltaTime;
        while (Math.Abs(cameraTransform.position.z - zPosition) > distance)
        {
            cameraTransform.Translate(0 , 0, distance, Space.World);
            yield return null;
        }
        currentStartZPosition = newZPosition;

    }
    public void MoveToNextStage()
    {
        newZPosition  =  currentStartZPosition + stageDistance;
        StartCoroutine(MoveCamera(newZPosition, cameraSpeed));
    }

    public void ResetCameraPosition()
    {
        currentCameraPosition.z = startZPosition;
        cameraTransform.position = currentCameraPosition;
        currentStartZPosition = startZPosition;
    }

    public void LockCamera()
    {
        isLocked = true;
    }
}
