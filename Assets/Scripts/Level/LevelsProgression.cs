using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Levels Progression")]
public class LevelsProgression : ScriptableObject
{

    [SerializeField] private List<GameObject> levelPrefabs;
    [SerializeField] private int lastLevel;

    public int LastLevel
    {
        get => lastLevel;
        set
        {
            if (value < LevelCount)
            {
                lastLevel = value;
            }
        }
    }

    public int LevelCount => levelPrefabs.Count;
    public GameObject GetLevel(int levelIndex)
    {
        return levelPrefabs[levelIndex];
    }
    
}
