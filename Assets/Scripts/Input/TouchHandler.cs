using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
    /// <summary>
    /// Обработка ввода игрока
    /// </summary>
public class TouchHandler : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    [SerializeField] private Path path;
    
    public Path Path
    {
        set => path = value;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        
        path.OnStartPathDrawing(eventData.pointerCurrentRaycast.worldPosition);
        OnDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
       
        if (eventData.position.x < Screen.width && eventData.position.y < Screen.height &&
            eventData.position.x > 0 && eventData.position.y > 0)
        { 
            path.AddPoint(eventData.pointerCurrentRaycast.worldPosition + new Vector3(0f, 0.1f, 0f));
        }
        else
        {
            path.StopDrawing();
        }
        
       
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        path.StopDrawing();
        
    }
}
